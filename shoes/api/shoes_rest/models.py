from django.db import models


class ShoeVo(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    closet_name = models.CharField(max_length=100)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=20)
    model_name = models.CharField(max_length=50)
    color = models.CharField(max_length=30)
    picture_url = models.URLField()
    wardrobe_bin = models.PositiveIntegerField()

    bin = models.ForeignKey(
        ShoeVo,
        related_name="shoes",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"{self.manufacturer} {self.model_name} - {self.color} Shoe"
