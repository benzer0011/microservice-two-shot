from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoe, ShoeVo


class ShoeVoEncoder(ModelEncoder):
    model = ShoeVo
    properties = ["import_href", "closet_name", "id"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "wardrobe_bin",
        "bin",
        "id",
    ]
    encoders = {"bin": ShoeVoEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            bin = ShoeVo.objects.get(id=content["bin"])
            content["bin"] = bin
        except ShoeVo.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_show_shoes(request, pk):
    if request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse({"message": "Deleted shoe"})
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
