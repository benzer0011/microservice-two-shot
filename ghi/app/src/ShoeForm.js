import React, { useEffect, useState } from 'react';
function ShoeForm() {
    const [bins, setBins] = useState([])
    const [formData, setFormData] = useState({
        manufacturer: '',
        model_name: '',
        color: '',
        picture_url: '',
        wardrobe_bin: 0,
        bin: 0,
    })
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }
    useEffect(() => {
        fetchData();
    }, [formData]);
    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8080/api/shoes/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {

            setFormData({
                manufacturer: '',
                model_name: '',
                color: '', picture_url: '',
                wardrobe_bin: 0,
                bin: 0,
            });

        }

    }
    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;


        setFormData({
            ...formData,

            [inputName]: value
        });
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">

                        <div className="form-floating mb-3">
                            {/* <!-- Now, each field in our form references the same function --> */}
                            <input onChange={handleFormChange} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="model name" required type="text" name="model_name" id="model_name" className="form-control" />
                            <label htmlFor="model name">Model name</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">color</label>
                        </div>

                        <div className="mb-3">
                            <label htmlFor="picture_url">Picture url</label>
                            <textarea onChange={handleFormChange} className="form-control" id="picture_url" rows="3" name="picture_url" className="form-control"></textarea>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Wardrobe bin" required type="number" name="wardrobe_bin" id="wardrobe_bin" className="form-control" />
                            <label htmlFor="Wardrobe bin">Wardrobe bin</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} required name="bin" id="bin" className="form-select">
                                <option value="">Choose a Bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.id} value={bin.id}>{bin.bin_number}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )


}
export default ShoeForm;
