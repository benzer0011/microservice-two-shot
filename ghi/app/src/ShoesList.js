import { useState, useEffect } from 'react';

function ShoesList() {
    const [shoes, setShoes] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/shoes');
        if (response.ok) {
            const { shoes } = await response.json();
            setShoes(shoes);
        } else {
            console.error('An error occurred fetching the data');
        }
    };

    const handleDelete = async (shoeId) => {
        try {
            const response = await fetch(`http://localhost:8080/api/shoes/${shoeId}`, {
                method: 'DELETE',
            });

            if (response.ok) {
                setShoes((delShoes) => delShoes.filter((shoe) => shoe.id !== shoeId));
            } else {
                console.error('An error occurred while deleting the shoe');
            }
        } catch (error) {
            console.error('An error occurred while deleting the shoe', error);
        }
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <div className="my-5 container">
            <div className="row">
                <h1>All Shoes</h1>

                <table className="table table-striped m-3">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Manufacturer</th>
                            <th>Model Name</th>
                            <th>Color</th>
                            <th>Bin</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {shoes.map((shoe) => (
                            <tr key={shoe.href}>
                                <td>{shoe.id}</td>
                                <td>{shoe.manufacturer}</td>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.color}</td>
                                <td>{shoe.bin.id}</td>
                                <td>
                                    <button onClick={() => handleDelete(shoe.id)}>Delete</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default ShoesList;
