from django.db import models

# Create your models here.
class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    picture_url = models.URLField()
    location_in_wardrobe = models.CharField(max_length=100)
