from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Hat

# Create your views here.
class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location_in_wardrobe"
    ]


@require_http_methods(["GET"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder = HatListEncoder,
            safe=False
        )

# @require_http_methods(["POST"])
# def api_create_hat(request):
#     if request.method == "POST":
#         try:
#             content = json.loads(request.body)
#             hat = Hat.objects.create(**content)
#             return JsonResponse(
#             hat,
#             encoder=HatListEncoder,
#             safe=False,
#         )
#         except:

# @require_http_methods(["DELETE"])
# def api_delete_hat(request, hat_id):
#     try:
#         shoe = Hat.objects.get(pk=hat_id)
#         shoe.delete()
#         return JsonResponse({"message": "Shoe deleted successfully"})
#     except Hat.DoesNotExist:
#         return JsonResponse(
#             {"message": "Invalid shoe id"},
#             status=400,
#         )
